Ext.define('Pertemuan.view.treelist.TreeList', {
    extend: 'Ext.grid.Tree',
    xtype :'tree-list',
    requires: [
        'Ext.grid.plugin.MultiSelection'
    ],

    
    cls: 'demo-solid-background',
    shadow: true,

    viewModel: {
        type: 'tree-list'
    },

    bind: '{navItems}',

    listeners: {
        itemtap:function ( me, index, target, record, e, eOpts ){
            detailtree = Ext.getCmp('detailtree'); 
           //detailtree.setHtml("Anda Memilih "+record.data.text);
           treedata = Ext.getStore('personnel');
           treedata.filter('npm'+record.data.text);
            //alert("Anda Memilih"+record.data.text);
        }
    }
});