Ext.define('Pertemuan.view.treelist.TreePanel', {
    extend: 'Ext.Container',
    xtype :'tree-panel',

    requires: [
     'Ext.layout.HBox',
     'Pertemuan.view.treelist.TreeList',
     'Pertemuan.view.setting.BasicDataView'
     ],

    layout: {
        type: 'hbox',
        pack: 'center',
        align: 'stretch'
    },
    margin: '0 10',
    defaults: {
        margin: '0 0 10 0',
        bodyPadding: 10
    },
    items: [
        { 
            xtype:'tree-list',
            flex:1

        },
        {
            xtype:'panel',
            id:'detailtree',
            flex:1, 
            items:[
            {
                xtype:'basicdataview'
            }
            ] 
            //html:'belum ada terpilih'  
        }
    ]

})