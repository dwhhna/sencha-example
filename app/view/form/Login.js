Ext.define('Pertemuan.view.form.Login', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Password'
        
    ],
    shadow: true,
    cls: 'demo-so lid-background',
    xtype: 'Login',
    controller: 'login',
    id: 'login',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldsetlogin',
            title: 'Login Form',
            //instructions: 'Please enter the information above.',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'username',
                    label: 'username',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'passwordfield',
                    revealable: true,
                    name : 'password',
                    label: 'Password',
                    clearIcon: true
                },
                   
            ]
        },
        
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'Login',
                    ui: 'action',
                   // scope: this,
                    hasDisabled: false,
                    handler: 'onLogin',
                }
                
            ]
        }
    ]
});