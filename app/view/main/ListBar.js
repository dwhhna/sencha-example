 /**
 * This view is an example list of people.
 */
Ext.define('Pertemuan.view.main.ListBar', {
    extend: 'Ext.grid.Grid',
    xtype: 'listbar',
    title: 'List Bar', 
    requires: [
        'Ext.grid.plugin.Editable',
    ],
    plugins: [{
        type: 'grideditable'
    }],

    bind: '{bar}',
    viewModel: {
        stores: {
            bar: {
                type: 'bar'
            }
        }
    },    
    columns: [
        { text: 'name', dataIndex: 'name', width: 200, editable:true },
        { text: 'g1', dataIndex: 'g1', width: 200, editable:true },
        { text: 'g2', dataIndex: 'g2', width: 200, editable:true },
        { text: 'g3', dataIndex: 'g3', width: 200, editable:true },
        { text: 'g4', dataIndex: 'g4', width: 200, editable:true },
        { text: 'g5', dataIndex: 'g5', width: 200, editable:true },
        { text: 'g6', dataIndex: 'g6', width: 200, editable:true }
    ],
    listeners: {
        select: 'onBarItemSelected'
    }
});
